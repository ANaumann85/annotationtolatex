import os.path
from .analyze import getAnnotatedLines

def annotate(pdfFile, destFolder, src=None):
  """ reads the sources files and stores the annotated files in the destination folder
  """
  lines = getAnnotatedLines(pdfFile, src)
  # collect all files
  # TODO: how does this work with more complicated file structures
  files = {}
  for line in lines:
    inFile = line['Input'][0]
    inBase = os.path.basename(inFile)
    inLines = files.get(inBase)
    if inLines is None:
      inLines = open(inFile, 'r').readlines()
      files[inBase] = inLines
    # the lines are only estimates
    # thus search the words in 
    # the lines before and after
    minLine = max(min(line['Line'])-1,1)
    maxLine = min(max(line['Line'])+1, len(inLines)+1)
    # convert to zero based
    minLine -= 1
    maxLine -= 1
    word = ' '.join([x[4] for x in line['words']])
    detectedLine = False
    for lNr in range(minLine, maxLine+1):
      if word in inLines[lNr]:
        inLines[lNr] = inLines[lNr].replace(word, f'\\todo{{annotated}}{word}')
        detectedLine = True
        break
    if not detectedLine:
      print(f'could not find {word} in {inFile} between lines {minLine+1} - {maxLine+1}')
  for fName, fVal in files.items():
    with open(f'{destFolder}/{fName}', 'w') as dest:
      dest.write(''.join(fVal))
# vim:set et sw=2 ts=2:
