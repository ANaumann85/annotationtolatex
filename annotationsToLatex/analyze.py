from subprocess import run
import sys
import os.path
import fitz

def getAnnotations(pdfFile):
  """ extracts all annotations from the pdf file

  The annotations are a list, where every entry is a dict containing
    - page: the page number (1-based)
    - rect: the annotations rectangle
    - type: the annotation type as tuple(nr, string) (underline, strikethrough..)
  """
  doc = fitz.open(pdfFile)
  annotations = None
  if doc.has_annots():
    annotations = []
    for pageNr, page in enumerate(doc):
      curAnnotations = [{'page' : pageNr+1, 'rect' : x.rect, 'type' : x.type} for x in page.annots()]
      annotations += curAnnotations
  return annotations, doc

def getLatexPosition(pdfFile, pageNr, x, y, src=None):
  pdfFile = os.path.split(pdfFile)
  pdfDir = pdfFile[0]
  pdfFile = pdfFile[1]
  cmd = ['synctex', 'edit', '-o', f'{pageNr}:{x}:{y}:{pdfFile}']
  if src is not None:
    cmd += ['-d', src]
  syncRes = run(cmd, capture_output=True, cwd=pdfDir)
  if syncRes.returncode != 0:
    errMsg = syncRes.stderr.decode(sys.getdefaultencoding())
    raise RuntimeError(f'synctex went wrong. The error message is {errMsg}')
  result = syncRes.stdout.decode(sys.getdefaultencoding())
  result = result.splitlines()
  # populate the result dict with some expected entries
  resultDict = { n : [] for n in ['Output', 'Input', 'Line', 'Column', 'Offset', 'Context'] }
  contentMode = False
  # decompose the results
  for line in result:
    # search for start or end
    if line == 'SyncTeX result begin':
      contentMode = True
      continue
    if contentMode:
      if line == 'SyncTeX result end':
        contentMode = False
        break
      nameVal = line.split(':')
      resultDict[nameVal[0]].append(nameVal[1])
  if len(resultDict['Line']) == 0:
    raise RuntimeError(f'did not find the lines in the result string  {result}')
  if contentMode:
    raise RuntimeError(f'could not find the end of the results in {result}')
  resultDict['Line'] = [int(x) for x in resultDict['Line']]
  return resultDict

def getAnnotatedLines(pdfFile, src=None):
  """ returns the estimated lines in the pdf file
  """
  annotations, doc = getAnnotations(pdfFile)
  lines = []
  for annotation in annotations:
    pageNr = annotation['page']
    rect = annotation['rect']
    # the left top of an annotation might not match the correct row
    # instead find intersection of the bounding box with text and
    # try to infer the underlined text
    annoBox = fitz.Rect(rect)
    page = doc[pageNr-1]
    pageText = page.get_text('words', clip=annoBox)
    firstWord = pageText[0]
    # use the left most x direction
    x = firstWord[0] #rect[0]
    # use the center in y direction
    y = 0.5*(firstWord[1] + firstWord[3])
    #annoType = annotation['type'] # TODO: use the type for a better point
    resultDict = getLatexPosition(pdfFile, pageNr, x, y, src)
    resultDict['words'] = pageText
    lines.append(resultDict)
  return lines

# vim:set et sw=2 ts=2:
