# Annotation to pdf

Annotation to pdf aims to transfer annotations from pdf files to LaTex files via synctex.

The basis is the [python binding](https://pymupdf.readthedocs.io/en/latest/) to [mupdf](https://mupdf.com/).
