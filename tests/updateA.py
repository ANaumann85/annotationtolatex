import sys
sys.path.append('..')
from annotationsToLatex import update

if __name__ == "__main__":
  folder = 'example/'
  pdfFile = f'{folder}/annotated/a.pdf'
  dest = f'{folder}/transformed/'
  update.annotate(pdfFile, dest, src='..')
# vim:set et sw=2 ts=2:
