import sys
sys.path.append('..')
from annotationsToLatex import analyze

if __name__ == "__main__":
  folder = 'example/'
  pdfFile = f'{folder}/annotated/a.pdf'
  annotations = analyze.getAnnotatedLines(pdfFile, src='..')
  print(annotations)
  if len(annotations) != 1:
      raise RuntimeError(f'wrong number of annotated lines in {pdfFile}. Got {len(annotations)}, expected 1')
  annotation = annotations[0]
  if annotation['Line'] != [119]:
    raise RuntimeError(f'expected line [119], but got {annotation["Line"]}')
  if len(annotation['words']) != 2:
    raise RuntimeError(f'two words, but got {len(annotation["words"])}')
# vim:set et sw=2 ts=2:
