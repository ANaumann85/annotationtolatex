import sys
sys.path.append('..')
from annotationsToLatex import analyze

if __name__ == "__main__":
  folder = './example'
  pdfFile = f'{folder}/annotated/a.pdf'
  annotations, _ = analyze.getAnnotations(pdfFile)
  if len(annotations) != 1:
      raise RuntimeError(f'wrong number of annotations in {pdfFile}. Got {len(annotations)}, expected 1')
# vim:set et sw=2 ts=2:
