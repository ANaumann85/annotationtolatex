import sys
sys.path.append('..')
from annotationsToLatex import analyze

def checkSingleLine():
  pageNr = 3
  x = 355.9739990234375
  y = 306.40771484375
  positions = analyze.getLatexPosition(pdfFile, pageNr, x, y, src='..')
  print(positions)
  if len(positions['Line']) != 1:
      raise RuntimeError(f'wrong number of detected lines in {pdfFile}. Got {len(positions["Line"])}, expected 1')
  line = positions['Line'][0]
  if line != 119:
    raise RuntimeError(f'expected line 119, but got {line}')

def checkMulipleLines():
  pageNr = 3
  x = 385.6550598144531
  y = 311.60154
  positions = analyze.getLatexPosition(pdfFile, pageNr, x, y, src='..')
  print(positions)
  lines = positions['Line']
  if len(lines) != 2:
      raise RuntimeError(f'wrong number of detected lines in {pdfFile}. Got {len(lines)}, expected 2')
  if lines != [119, 120]:
    raise RuntimeError(f'expected lines 119 and 120, but got {lines}')

if __name__ == "__main__":
  folder = 'example/'
  pdfFile = f'{folder}/annotated/a.pdf'
  checkSingleLine()
  checkMulipleLines()
# vim:set et sw=2 ts=2:
